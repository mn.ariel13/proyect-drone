<?php
    session_start();

    if( $_SESSION["state"] == FALSE ) {

        header("location:login");

        exit();

    }
    include "view/module/navbar.php";

?>
<head>
  <title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="view/assets/css/custom.css">
</head>
<div class="container-fluid bg-gray" id="accordion-style-1">
	<div class="container">
		<section>
			<div class="row">
				<div class="col-12">
					<h1 class="text-green mb-4 text-center">Sobre Nuestro Proyecto</h1>
				</div>
				<div class="col-10 mx-auto">
					<div class="accordion" id="accordionExample">
						<div class="card">
							<div class="card-header" id="headingOne">
								<h5 class="mb-0">
							<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							  <i class="fa fa-plus main"></i><i class="fa fa-angle-double-right mr-3"></i>Resumen
							</button>
						  </h5>
							</div>

							<div id="collapseOne" class="collapse show fade" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body">
                                El sistema de relevamiento con un dron que se implementará un servidor, con 
                                un análisis estratégico e inteligente para una trasferencia de archivos, basándose 
                                en algoritmos de fotogrametría. Contando con una plataforma inteligente para el procesamiento de datos
                                tomados para una mayor confortabilidad
                                y desempeño laboral, brindado un mejor servicio. Con un ahorro de tiempo, 
                                dando una mayor seguridad al personal de trabajo y poder llegar a puntos inaccesible.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h5 class="mb-0">
							<button class="btn btn-link collapsed btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							 <i class="fa fa-plus main"></i><i class="fa fa-angle-double-right mr-3"></i>Misión y Visión
							</button>
						  </h5>
							</div>
							<div id="collapseTwo" class="collapse fade" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                
                                <strong>Misión</strong><hr>
                                Contribuir eficazmente al profecional del area topografica, 
                                con calidad, excelencia en  servicios y soluciones de relevamiento topografico.
                                asi Asegurar la continuidad de la empresa basándonos en estrategias 
                                serias, que brinden confianza a los clientes y topografos. </br></br>
                                
                                
                                <strong>Visión</strong><hr>
                                Ser reconocidos mediante una empresa sólida, eficiente y confiable,
                                para dar soluciones de infraestructura de Tecnología electrónica, 
                                que se basara en el compromiso, satisfacción y crecimiento
                                profesional de su equipo, supere las expectativas de sus clientes
                                mediante una alta calidad de servicio e innovadoras soluciones tecnológicas
                                                                </div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree">
								<h5 class="mb-0">
							<button class="btn btn-link collapsed btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							  <i class="fa fa-plus main"></i><i class="fa fa-angle-double-right mr-3"></i>Tecnicas de Recoleccion
							</button>
						  </h5>
							</div>
							<div id="collapseThree" class="collapse fade" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                se usa en un servidor para la interfaz del usuario el cual contiene todos los protocolos que se nesecita para trabajar en red 
                                asi a su vez permitir la elboracion de un ortomosaico con calidad y eficacia
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingFour">
								<h5 class="mb-0">
							<button class="btn btn-link collapsed btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							  <i class="fa fa-plus main"></i><i class="fa fa-angle-double-right mr-3"></i>Alcances
							</button>
						  </h5>
							</div>
							<div id="collapseFour" class="collapse fade" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                El sistema de relevamiento con un dron que se implementará un servidor, con 
                                un análisis estratégico e inteligente para una trasferencia de archivos, basándose 
                                en algoritmos de fotogrametría. Contando con una plataforma inteligente para el procesamiento de datos
                                tomados para una mayor confortabilidad
                                y desempeño laboral, brindado un mejor servicio. Con un ahorro de tiempo, 
                                dando una mayor seguridad al personal de trabajo y poder llegar a puntos inaccesible.
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</section>
	</div>
</div>