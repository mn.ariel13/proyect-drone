<?php
    session_start();

    if( $_SESSION["state"] == FALSE ) {

        header("location:login");

        exit();

    }
    include "view/module/navbar.php";

?>
<head>
  <title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="view/assets/css/custom.css">
</head>
<div class="container mt-30">
    <h2 class="text-center text">DISEÑO E IMPLEMENTACIÓN DE UN SERVIDOR PARA LA TRASFERENCIA DE DATOS, APLICADO PARA UN DRON TOPOGRÁFICO</h2>
    <div class="row mt-30">
        <div class="col-sm-12 col-md-6">
            <div class="box20 blue">
                <img src="view/assets/img/mejillones.png" alt="">
                <div class="box-content">
                    <i class="fas fa-university circle-icon"></i>
                        <h3 class="title">INSTITUTO TECNOLÓGICO INDUSTRIAL COMERCIAL PUERTO DE MEJILLONES</h3>
                        <!-- <p><a class="button" href="/more">Learn More</a></p> -->
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="box20 red">
                <img src="view/assets/img/elect.png" alt="">
                <div class="box-content">
                    <i class="fas fa-graduation-cap circle-icon"></i>
                    <h3 class="title"> CARRERA TECNICO SUPERIOR EN ELECTRONICA</h3>
                    <!-- <p><a class="button" href="/more">Learn More</a></p> -->
                </div>
            </div>
        </div>
    </div>
</div>