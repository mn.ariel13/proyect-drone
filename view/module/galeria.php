<?php
    session_start();

    if( $_SESSION["state"] == FALSE ) {

        header("location:login");

        exit();

    }
    include "view/module/navbar.php";

?>
<head>
  <title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="view/assets/css/custom.css">
</head>

<?php
$image="view/assets/drone-img/DJI_0084.JPG";
// echo "<img class='visible-lg img-responsive' src='".$image."' border='0' width='300' height='100'>"; 
// We obtain the coordinated of the line
function getCoord( $expr ) {
	$expr_p = explode( '/', $expr );
	return $expr_p[0] / $expr_p[1];
}

// // If an image is not uploaded
if( empty( $image ) ) {
	header( 'Location: /?error=2' );
	exit();
}

// If an image type is not jpeg

// We use a temporary file path 
$img = $image;

// // We get the data
$exif = exif_read_data( $img, 0, true );
// var_dump($exif);

// // If there is no GPS branch 
if( empty( $exif['GPS'] ) ) {
    header( 'Location: /?error=2' );
	exit();
}

// Latitude
$latitude['degrees'] = getCoord( $exif['GPS']['GPSLatitude'][0] );
$latitude['minutes'] = getCoord( $exif['GPS']['GPSLatitude'][1] );
$latitude['seconds'] = getCoord( $exif['GPS']['GPSLatitude'][2] );

$latitude['minutes'] += 60 * ($latitude['degrees'] - floor($latitude['degrees']));
$latitude['degrees'] = floor($latitude['degrees']);

$latitude['seconds'] += 60 * ($latitude['minutes'] - floor($latitude['minutes']));
$latitude['minutes'] = floor($latitude['minutes']);

// Longitude
$longitude['degrees'] = getCoord( $exif['GPS']['GPSLongitude'][0] );
$longitude['minutes'] = getCoord( $exif['GPS']['GPSLongitude'][1] );
$longitude['seconds'] = getCoord( $exif['GPS']['GPSLongitude'][2] );

$longitude['minutes'] += 60 * ($longitude['degrees'] - floor($longitude['degrees']));
$longitude['degrees'] = floor($longitude['degrees']);

$longitude['seconds'] += 60 * ($longitude['minutes'] - floor($longitude['minutes']));
$longitude['minutes'] = floor($longitude['minutes']);

?>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h2 class="text-green">Vista de la Imagen en tiempo real</h2>
            <?php 
            echo "<img class='img-fluid img-drone' src='".$image."' >"; 
            ?>  
        </div>
        <div class="col-sm-4">
            <h2 class="text-green">Coordenadas y GEO</h2>
            <p class="text-p">
                Latitude: 
                <?=$exif['GPS']['GPSLatitudeRef'] == 'S' ? '-' : '' ?>
                <?=$latitude['degrees']?><sup>o</sup>
                <?=$latitude['minutes'] ?>'
                <?=$latitude['seconds'] ?>''
            </p>
            
            <p class="text-p">
                Longitude:
                <?=$exif['GPS']['GPSLongitudeRef'] == 'W' ? '-' : '' ?>
                <?=$longitude['degrees']?><sup>o</sup>
                <?=$longitude['minutes'] ?>'
                <?=$longitude['seconds'] ?>''
            </p>
            
            <p>
                <a class="btn button-v" href="https://maps.google.com/maps?q=<?=$exif['GPS']['GPSLatitudeRef'] == 'S' ? '-' : '' ?><?=$latitude['degrees']?>+<?=$latitude['minutes']?>'+<?=$latitude['seconds']?>'',+<?=$exif['GPS']['GPSLongitudeRef'] == 'W' ? '-' : '' ?><?=$longitude['degrees']?>+<?=$longitude['minutes']?>'+<?=$longitude['seconds']?>''" target="_blank">Mostrar en el Mapa</a>
            </p>
        </div>
    </div>
</div>

    
