<nav class="navbar navbar-dark navbar-expand-sm">
  <a class="navbar-brand as" href="home">
    <img src="view/assets/img/drone.png" width="30" height="30" alt="logo">
    Drone
  </a>
  <a class="navbar-brand" href="proyecto">
    <!-- <img src="view/assets/img/drone.png" width="30" height="30" alt="logo"> -->
    Nuestro Proyecto
  </a>
  <a class="navbar-brand" href="galeria">
    <!-- <img src="view/assets/img/drone.png" width="30" height="30" alt="logo"> -->
    Galeria
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-4" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse justify-content-end" id="navbar-list-4">
    <ul class="navbar-nav" style="right:auto; left: auto;">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="view/assets/img/user.png" width="60" height="60" class="rounded-circle">
        </a>
        <div class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Mis Imagenes</a>
          <a class="dropdown-item" href="#">Mi Perfil</a>
          <a class="dropdown-item" href="logout">Cerrar Sesion</a>
        </div>
      </li>   
    </ul>
  </div>
</nav>

