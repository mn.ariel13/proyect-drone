
<!--=====================================
FORMULARIO DE INGRESO         
======================================-->
<head>
  <title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
  <!--Fontawesome CDN-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!--Custom styles-->
  <link rel="stylesheet" type="text/css" href="view/assets/css/custom-styles.css">
</head>
<div class="container logo">
  <div class="d-flex justify-content-center h-100">
    <div class="card">
      <div class="card-header">
        <h3 class="text-center h3-2" >Bienvenido a Drone</h3>
      </div>
      <div class="card-body">
        <form method="post" name="form_drone">
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="usuario" name="user_login" id="login" required>
            
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" class="form-control" placeholder="contraseña" name="user_password" id="password" required>
          </div>
          <div class="form-group text-center">
            <input type="submit" value="Ingresar" id="btnLogin" class="btn float-center login_btn">
            <?php

                $ingreso = new Login();
                $ingreso -> loginController();
                                        
            ?>
          </div>
        </form>
      </div>
      <div class="card-footer">
        <div class="d-flex justify-content-center links">
          No tienes cuenta?<a href="#">Crea tu cuenta</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/container-->

<script>
	var form = $("#formLogin")

	if (form[0].checkValidity() === false) {
	event.preventDefault()
	event.stopPropagation()
	}

	form.addClass('was-validated');
	})
</script>


<!--====  Fin de FORMULARIO DE INGRESO  ====-->

