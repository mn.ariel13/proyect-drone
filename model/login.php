<?php  

	require_once 'conexion.php';

	class LoginModels {

		public function loginModel( $datosController ) {

	        try {
	                    
	            $db = new Connection();
	            $db = $db->connect();

	           	// $answer = $db->prepare(" SELECT * FROM logear_encargado( :usuario, :password ) ");
	           	$answer = $db->prepare(" SELECT * FROM user WHERE login_user=:login_user AND password_user=:password_user ");

	           	$answer -> bindParam(":login_user", $datosController["user"], PDO::PARAM_STR);
	           	$answer -> bindParam(":password_user", $datosController["password"], PDO::PARAM_STR);

	           	$answer -> execute();

				return $answer -> fetch();

	            $answer -> close();

	        }catch(PDOException $e){

	        	echo '{"error" : {"text":'.$e->getMessage().'}';

	        }
		}

		public function logoutModel( $id_process ) {

	        try {
	                    
	            $db = new Connection();
	            $db = $db->connect();

	            $answer = $db->prepare(" SELECT * FROM logout_encargado( :id_process ) ");

	           	$answer -> bindParam(":id_process", $id_process, PDO::PARAM_STR);

	           	$answer -> execute();

				return $answer -> fetch();

	            $answer -> close();

	        }catch(PDOException $e){

	            echo '{"error" : {"text":'.$e->getMessage().'}';

	        }
		}
	}
?>