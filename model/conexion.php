<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/Proyecto Drone/model/db_config.php';

/** 
 * La sentencia "require_once" que nos va a permitir incluir y hacer uso de un archivo externo,
 * Se emplea la variable "$_SERVER" que es un array que almacena información del servidor donde se está ejecutando nuestra aplicación.
 * Al array le pasamos un índice en este caso 'DOCUMENT_ROOT', a través del cual  estaremos solicitando al directorio raíz de 
 * documentos del servidor. A continuación, usamos un punto ( . ) para concatenar, y entre comillas simples ( ' ) escribiremos la ruta 
 * en donde se encuentra el archivo requerido, que será "configuration.php"
*/

class Connection{

    private $hostname = HOST_NAME;
    private $database = DATABASE_NAME;
    private $user = USER;
    private $password = PASSWORD;
    private $port = PORT;
    private $conexion;

  public function connect(){

        try {
            
            $opciones = array(
                        PDO::ATTR_PERSISTENT=>true, 
                        PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION 
                        );

            $this->conexion=new PDO('mysql:host='.$this->hostname.';port='.$this->port.';dbname='.$this->database, 
                                    $this->user, $this->password, $opciones);
            
            echo 'Conexión Exitosa';
            
        } catch (PDOException $error) {
            
            echo "¡ERROR: !".$error->getMessage();
            die();
            
        }
    
        
        return $this->conexion;

  }
   
}
